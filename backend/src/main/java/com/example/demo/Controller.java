package com.example.demo;

import com.example.demo.dtos.MessageDTO;
import com.example.demo.types.FractionMessage;
import com.example.demo.types.HexMessage;
import com.example.demo.types.StringMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://127.0.0.1:5500")
public class Controller {
    static MessageQueue messageQueue = new MessageQueue();

    public  Message createMessage(int type, String message) {
        Message newMessage = new Message();

        switch (type) {
            case 1: // string
                newMessage = new StringMessage(message);
                break;
            case 2: // fraction
                newMessage = new FractionMessage(message);
                break;
            case 3: // Hex
                newMessage = new HexMessage(message);
                break;
            default:
                throw new IllegalArgumentException("Invalid message type: " + type);
        }

        return newMessage;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<String> helloWorld() {

        return ResponseEntity.ok("{\"message\": \"GET request handled successfully\"}");
    }

    @RequestMapping(value = "/api/", method = RequestMethod.POST)
    public ResponseEntity<String> receiveMessage(@RequestBody MessageDTO message) {
//        System.out.println(message.getType());
//        newMessage.print();
//        return ResponseEntity.status(405).body("Not Valid message or type");
        Message newMessage= createMessage(message.getType(),message.getContent());
        if (newMessage.isValidMessage()) {
            messageQueue.addMessage(newMessage);
            //TODO : send to server2 for process

            messageQueue.printAllMessages();
            String responseJson = "{\"message\": \"" + message.getContent() + "\"}";

            return ResponseEntity.ok(responseJson);
        } else {
            // Handle the case where POST is not supported
            return ResponseEntity.status(405).body("Not Valid message or type");
        }

    }


}
