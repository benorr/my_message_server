package com.example.demo.types;

import com.example.demo.Message;



public class StringMessage extends Message {
    private  String content;

    public StringMessage(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    @Override
    public void print(){
        System.out.println("String Message: "+content);
    }

    @Override
    public boolean isValidMessage() {
        return isOnlyNumber(content);
    }
}
