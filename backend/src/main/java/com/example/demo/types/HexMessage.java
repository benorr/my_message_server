package com.example.demo.types;

import com.example.demo.Message;

public class HexMessage extends Message {
    private final String value;

    public HexMessage(String value) {
        this.value = value;
    }

    @Override
    public boolean isValidMessage() {

        if (value == null || value.isEmpty()) {
            return false; // Null or empty string is not a valid hex message
        }
        for (char c : value.toCharArray()) {
            if (!isHexDigit(c)) {
                return false; // Non-hexadecimal character found
            }
        }
        return true; // All characters are valid hexadecimal digitsreturn false;
    }

    @Override
    public void print(){
        System.out.println("Hex Message: "+value);
    }

    private static boolean isHexDigit(char c) {

        return (c >= '0' && c <= '9') ||
                (c >= 'A' && c <= 'F') ||
                (c >= 'a' && c <= 'f');
    }
}
