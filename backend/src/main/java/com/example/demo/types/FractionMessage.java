package com.example.demo.types;

import com.example.demo.Message;

public class FractionMessage extends Message {
    private String  numerator;
    private String denominator;
    public FractionMessage(String fractionString) {
        String[] parts = fractionString.split("/");

        if (parts.length == 2) {

                numerator = parts[0];
                denominator = parts[1];

        } else {
            numerator = "0"; // Default value
            denominator = "0"; // Default value
        }
    }

    @Override
    public boolean isValidMessage() {
        return isOnlyNumber(numerator)
        &&isOnlyNumber(denominator)
        &&denominator.charAt(0)!='0'&&numerator.charAt(0)!='0';
    }
    @Override
    public void print(){
        System.out.println("Fraction Message: "+numerator + "\""+denominator);
    }
}
