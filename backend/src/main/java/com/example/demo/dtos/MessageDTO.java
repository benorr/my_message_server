package com.example.demo.dtos;

public class MessageDTO {
    private int type;
    private String content;

    // Default constructor
    public MessageDTO() {
    }

    // Constructor with parameters
    public MessageDTO(int type, String content) {
        this.type = type;
        this.content = content;
    }

    // Getter and setter methods
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
