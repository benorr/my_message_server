package com.example.demo;

public  class MessageValidationService {

    public boolean isOnlyNumber(String string){
        if (string == null || string.isEmpty()) {
            return false; // Null or empty message is invalid
        }

        // Check that all characters are digits (numbers)
        for (char c : string.toCharArray()) {
            if (c < '0' || c > '9') {
                return false; // Non-digit character found
            }
        }
        return true; // All characters are numbers
    }
    public boolean isValidMessage() {
        return false;
    }
}