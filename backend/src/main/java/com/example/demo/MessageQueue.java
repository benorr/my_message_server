package com.example.demo;

import java.util.LinkedList;
import java.util.Queue;

public class MessageQueue {

    private Queue<Message> messageQueue;
    private int size;

    public MessageQueue() {
        // Initialize the queue (you can choose other implementations if needed)
        messageQueue = new LinkedList<>();
        size=0;
    }

    public void addMessage(Message message) {
        // Add the message to the queue
        size=size<=10?size++:0;
        messageQueue.add(message);

    }
    public void removedMessage () {
        // Removing an element from the queue
        Message removedMessage = messageQueue.poll();
        if (removedMessage != null) {
            // Element was removed successfully
            System.out.println("Removed Message: " + removedMessage);
        } else {
            // Queue was empty
            System.out.println("Queue is empty");
        }
    }
    public void setMessageQueue(Queue<Message> messageQueue) {
        this.messageQueue = messageQueue;
    }
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Queue<Message> getMessageQueue() {
        return messageQueue;
    }


    public void printAllMessages() {
        System.out.println("Messages in the queue:");
        for (Message message : messageQueue) {
            message.print();
        }
    }


}
