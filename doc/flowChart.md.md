# Flowchart for Mission Requirements

```plaintext
Start

Webapp: Receive message input
|
v
Webapp: Send message to Server 1
|
v
Server 1: Receive message
|
v
Server 1: Validate message
|
v
Server 1: Is message valid?
|
|--- No ---|
|          v
|  Server 1: Handle invalid message (e.g., log, throw exception)
|          |
|          v
|  End (for invalid message)
|
|--- Yes ---|
|          v
|  Server 1: Insert valid message into persistent queue
|          |
v          |
Server 2: Process messages from the queue
|
v
Server 2: Is there a message to process?
|
|--- No ---|
|          v
|  End (for no messages)
|
|--- Yes ---|
|          v
|  Server 2: Get next message from the queue
|          |
v          |
Server 2: Print message in its original form
|
v
Server 2: Calculate summation based on message type
|
v
Server 2: Send summation to configured output mechanism(s)
|
v
Server 2: Is there another message to process?
|
|--- No ---|
|          v
|  End (for no more messages)
|
|--- Yes ---|
|          v
|  Return to "Server 2: Get next message from the queue"
|          |
v          |
End