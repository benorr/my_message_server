const getButton = document.getElementById('getButton');
const postButton = document.getElementById('postButton');
const resultDiv = document.getElementById('result');
const messageInput = document.getElementById("IMessage");
const mySelect = document.getElementById("mySelect");
// Function to make a GET request
function makeGETRequest() {
    fetch('http://localhost:8080/')
        .then(response => response.json())
        .then(data => {
            resultDiv.innerHTML = `<h2>GET Response:</h2><pre>${data.message}</pre>`;
        })
        .catch(error => {
            resultDiv.innerHTML = `<h2>Error:</h2><p>${error.message}</p>`;
        });
}

// Function to make a POST request
function makePOSTRequest() {
    const inputValue = document.getElementById("IMessage").value; // Get input value
    const selectValue = document.getElementById("mySelect").value; // Get select value

    const postData = {
        content: inputValue, 
        type: parseInt(selectValue)
    };
    // console.log(postData);
    if (selectValue && inputValue) {
        fetch(`http://localhost:8080/api/`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postData)
    })
        .then(response => response.json())
        .then(data => {
            // Assuming the response has a 'message' property
            resultDiv.innerHTML += `<h2>POST Response:</h2><pre>${data.message}</pre>`;
        })
        .catch(error => {
            alert(error.message); // Display error message as an alert
        });
    } else {
        alert("Please enter a valid response");
    }
}


// Event listeners for the buttons
getButton.addEventListener('click', makeGETRequest);
postButton.addEventListener('click', makePOSTRequest);
